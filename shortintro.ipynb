{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# A short introduction to SCAM\n",
    "\n",
    "SCAM is a Julia package for doing symbolic computations with an emphasis on algebra and mathematics."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "using SCAM"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Symbolic Expressions\n",
    "\n",
    "The easiest way to introduce a symbolic expression is with the `@symbolic` macro.  For example:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "@symbolic 1+x+5+y+x"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "@symbolic ∂{sin(cos(x)^2),x}"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "@symbolic ∫{x^2*cos(x),x}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Sometimes it is useful to tag an atom in the expression with a specific type:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "@symbolic cos(x) x::Real"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "(If the type of an atom is not specified, SCAM assigns it the *bottom* type, the elements of which are of *every* type.  The effect of this is that SCAM is free to make any assumptions it likes about the variables represented by such atoms.  Usually, this is a convenience.)\n",
    "\n",
    "Symbolic expressions can be interpolated into the definitions of other expressions:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "xplusone=@symbolic x+1"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "@symbolic $(xplusone)^2"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "`SCAM.asexpr` can covert many symbolic expresssions into Julia expressions.\n",
    "\n",
    "The main exception to this is that SCAM abuses Julia's type parameterisation notation to provide alternative \"application-like\" symbolic expressions, using the curly rather than round brackets.  (See for example the expressions involving the derivative and integral, above.)\n",
    "\n",
    "For example, we could interpolate `xplusone` into the definiton of a Julia function:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "@eval plusone(x)=$(SCAM.asexpr(xplusone))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "plusone(3)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Simplification\n",
    "\n",
    "`SCAM.simplify` will automatically apply a standard set of manipulations to \"simplify\" a symbolic expression:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "SCAM.simplify(@symbolic 1+x+5+y+x)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "More conveniently, the `@s` macro allows the definition of a symbolic expression, immediately followed by its simplification:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "@s 1+x+5+y+x"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Most of the computer algebra capabilities of SCAM are available through `SCAM.simplify`.  For example:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "@s ∂{sin(cos(x)^2),x}"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "@s ∫{x^2*cos(x),x}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Rearranging expressions\n",
    "\n",
    "`expand` can be used to distribute products and powers over sums:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "P=@s 3*(1+a+x+y)*x*(1+a)^2"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "@s expand($P)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The expansion can be limited to a specified set of variables:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "@s expand{x,y}($P)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Or even some expressions:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "@s expand{sin(x),cos(x)}(∫{x^2*cos(x),x})"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "`mapcoeff{variables...}(expression)(function)` maps a `function` over the coefficients in an `expression` of the specified `variables`.\n",
    "\n",
    "For example, we can expand an expression in $x$ and $y$, and then independently expand each resulting coefficient:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "@s mapcoeff{x,y}(expand{x,y}($P))(expand)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## A little more on curly forms\n",
    "\n",
    "The main difference between normal applications, `f(x,...)`, and curly forms, `f{x,...}`, is that simplification does not, by default, recurse into the sub-arguments of a curly form.  See for example:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "@s f(x+x)+f{x+x}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Curly forms are often used to represent manipulations that are not straight-forward simplifications.  For example, `subst` can be used to perform substitution:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "@s subst{x,z+2}(x^2)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "And in places where immediate evalution might be undesirable.  For example in λ-expressions:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "doubleplusa=@s λ{a+x+x,x}"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "@s $(doubleplusa)(3)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Context Values\n",
    "\n",
    "SCAM maintains a \"context\" with respect to which it interprets expressions.  One aspect of this is the provision of a general dictionary of values.  These can be accessed by using the `contextvalue` curly form:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "@s contextvalue{MyContextValue}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Context values can be set using the `context` curly form:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "@s context{2*contextvalue{MyContextValue},MyContextValue,3}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "`context` introduces a sub-context in which the context value is set - the change is not global:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "begin\n",
    "\tA=@symbolic contextvalue{a}\n",
    "\tB=@symbolic contextvalue{b}\n",
    "\t\n",
    "\t@s context{f(context{f($A,$B),b,two},f($A,$B)),a,one}\n",
    "end"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Example - integration effort\n",
    "\n",
    "Eventually, it is planned to implement more sophisticated methods, however for now,\n",
    "SCAM uses a heuristic method for performing indefinite integration in a similar way that a human might integrate by hand, combining pattern matching known forms with search for appropriate substitutions, or applications of integration by parts.  (Even when more sophisticated methods are implemented, the heuristic method is likely to be tried first.)\n",
    "\n",
    "The problem is that the search could conceivably continue forever and that eventually the heuristic method will need to \"give up\".\n",
    "\n",
    "The following integral can be solved by a substitution, and then a number of applications of integration by parts, but, by default, this is more steps than the heuristic integrator is willing to spend:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "tricky_integral=@s ∫{sin(x)^6*cos(sin(x))*cos(x),x}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The effort that the heuristic integrator will spend is given by the `integration_effort` context value:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "@s contextvalue{integration_effort}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can try the integral again with an increased effort:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "@s context{$tricky_integral,integration_effort,200}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Example - Assumptions\n",
    "\n",
    "Another context value used by SCAM are the \"assumptions\", this stores a set of auxilliary facts with respect which SCAM will simplify expressions.  The `assume` form should be used for extending the assumptions:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "@s assume{assume{contextvalue{assumptions},x^2>0,a==b},b==0}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "SCAM refuses to compute the following integral because it doesn't have enough information about $a$:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "integral_needing_extra_assumptions=@symbolic expand{x}(∫{(x^2+a^2)^(5/2),x});"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "@s $integral_needing_extra_assumptions"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can help out by letting SCAM know that we have non-zero values of $a$ in mind:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "@s assume{$integral_needing_extra_assumptions,¬(a==0)}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "You might be disappointed that the $\\sqrt{a^2}$ terms have not been simplified further, but consider what happens when $a$ is *negative* and compare to the positive case.\n",
    "\n",
    "If we would like to tell SCAM that, in fact, $a$ is positive, it can simplify further:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "@s assume{$integral_needing_extra_assumptions,a>0}"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Julia 1.6.0",
   "language": "julia",
   "name": "julia-1.6"
  },
  "language_info": {
   "file_extension": ".jl",
   "mimetype": "application/julia",
   "name": "julia",
   "version": "1.6.0"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
